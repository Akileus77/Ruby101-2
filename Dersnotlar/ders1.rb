
# print "Merhaba"
# puts 
# puts "Dünya"
# a = "Anan" 
# puts a
# array = ["El","Bacak","köfte"]
# puts array
# print array
# puts
# puts array[0]
#nil 
# sayı = 1 #integer 
# sayı2 = 1.0 #float 
# metin = "selam" #string
# metin2 = "" 
# metin3= 3
# puts metin3.nil? 
# metine sorduk metin3 boş mu ? çıktı olarak true yada false
#   array = "isim"
#  a = "emir"
# puts " #{array}: #{a} " #çift syntax yoksa içeri kod çağrılmaz 
# puts array 

# dogru = [0,''," ", "false", :nil]
#  yanlis = [nil, false]

# for d in dogru+yanlis do
#    puts !!d
#  end

#metotlar 
# def selamla
#   "selamla"
# end

# puts selamla
# puts selamla()
#  def topla(a, b)
#    a + b
#  end

#  puts topla(46, 808)
#  puts topla("patates", "monitör")

# def topla(ü, b)
#   a + b
# end
#  puts topla(2, 4)

#metinler

# metin = "Merhaba"
# puts metin.class
# sayi1 = 1
# puts sayi1.class

# ad = "ali"
# soyad = "patates"
# puts soyad.upcase
# puts "Merhaba benim adım #{ad.capitalize} #{soyad.upcase}"

#Karakter Sabitleri

# "\x100" # => @
# "\40" # => @
#metin metodları
# m1 = "ruby303"
# m2 = "Murat eğitim"
# puts m1.include? "ruby"
# metin = "Ruby 303"
# aranan = "Ruby "
# puts metin.downcase.include? aranan.downcase
# metin2 = "Selam"
# puts metin2.start_with? "S"
# puts metin2.end_with? "m"
# puts metin2.include? "e"

# array = ["a", 2, "3"]
# puts array[2] + array[2]

# metin = "patatesim var"
# puts metin.index  #geri anlatılacak diye
# metin = "Selam" 
# puts metin.reverse
# puts metin.start_with? metin.reverse 
# metin4 = "Ruby patates"
# puts metin4.swapcase
# puts metin4.upcase
# puts metin4.downcase

# metin5 = "Ben ekmek almaya gittim dönüşte ayağım kaydı düştüm."
# puts metin5.split.inspect[3, 50]
# #direk içindekini alır
