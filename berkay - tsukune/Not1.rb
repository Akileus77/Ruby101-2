# puts #bir alt satıra geçip yazdırır
# print #direk olduğu gibi yazdırır
# metin1 = "sadşlöaf" #String metin
# sayı1 = 1 #integer, tam sayı
# sayı2 = 2.39 # Float, ondalıklı
# puts sayı1
# puts metin1
# print sayı2
# puts "#{sayı2}"
# puts "Benim sayım şudur #{sayı2}"
# #ctrl + s
# puts ''
# bos = nil #boş yok 0
# puts bos
# puts sayı1.nil? #sorduk
# dizi = ["3", 4, "sadas"]#array #arrayler 0 değerinden başlar
# puts dizi[1]
#
# a = dizi[1]
# b = dizi[2]
# puts "#{a} #{b}"
#
#
# # Mantıksal
#  dogru = [0,''," ", "false", :nil]
#  yanlis = [nil, false]
#
#  for d in dogru+yanlis do
#    puts !!d
#  end
# #metodlar
# def topla(a, b) #topla fonk tanımladık sonra 2 değer verdik
#   a + b
# end
#
# puts topla(3, 6) # integer + integer = integer
# # puts topla("3", 6) #string + integer = hata
# puts topla("3", "3")#string + string = string
# puts topla(3.0, 4.2)#float + float = float
#
# def selamla(deger1)
#  "Merhaba #{deger1}"
# end
#
# puts selamla("Emir sus artık amk")
#
# # print topla([1,2,3],["Emir","in","aq","ü"])
#
# #metinler ve metin metodlar
# metin = "merhaba"
# puts metin.class
# puts sayı1.class
# puts sayı2.class
#
# ad = "berkay"
# soyad = "batates"
# puts "Merhaba benim adım #{ad.capitalize} #{soyad.upcase}"

#Karakter Sabitleri

# "\x100" # => @
#"\40" # => @
# puts "\u00FC" #ü
 #
 # m1 = "Ruby 303"
 # m2 = "Murat eğitim"
 # puts m2.include? "Ruby" #false
 # puts m2.include? "murat" #büyük küçük önemi fazla
 # puts m1.include? "303" #true
 # aranan = "rubY "
 # puts m1.downcase.include?  aranan.downcase
 # puts m1.start_with? "R"
 # puts m2.start_with? "Mu"
 # puts m1.end_with? "303"
 # puts m1.index "1"  #sonra bakılacak
 #
 # puts m1.downcase #hepsi küçük
 # puts m1.upcase #hepsi büyük
 # puts m1.swapcase #büyük küçük yer değiştirir
 # puts m1.reverse #tersine çeviriyor
 #
 # metin2 = "ekmek almaya gittim ayağım kaydı düştüm"
 # puts metin2.split.inspect #metin2 yi böl diziye çevir

 #Döngüler ve koşul ifadeleri
#  if else elsif
#    unless
#    case when
# while
#   each
#   until
#ctrl /

# print "ilk değer için 1 i girin, 2 değer için 2 girin :"
# a = gets.chomp.to_i
# if a == 1
#   puts "#{a} girdiğiniz sayı sonuç 1"
#   elsif a == 2
#   puts "#{a} girdiğiniz sayı sonuç 2"
#   else
#   puts "hata"
# end
# b = rand(1..5)
# puts b
# random = rand(1..6)
# case random
#   when 1
#     puts "değeriniz #{random}"
#   when 2
#     puts "değeriniz #{random}"
#   when 3
#       puts "değeriniz #{random}"
#   when 4
#       puts "değeriniz #{random}"
#   when 5
#       puts "değeriniz #{random}"
#   when 6
#       puts "değeriniz #{random}"
#     end
#
# var = 5
# unless var == 3  then
#   puts "#{var} değeri"
# end

#tip dönüşümleri
#.to_i
#.to_s
#.to_f

 #tip dönüşümleri karmaşık sayılar yuvarlama hatası ve sayilar




#bonus yüzdelik

a = rand(1..100) #1 ile 100 arasında random sayı



case a

  when 1..10 #%10 şans

  puts "%10"
  puts a
  when 11..30 #%20 şans

  puts "%20"
  puts a
  when 31..100 #%70 şans

  puts "%70"
  puts a
  end



 c = rand(1..100)


 case c

  when 1..50 #%50 şans

  puts "A %50"

  puts "#{c}" #Random değeri görmek için

  when 51..100 #%50 şans

  puts "b %50"

  puts "#{c}"

  end
